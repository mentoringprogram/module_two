﻿using System;
using System.IO;
using DirectoryVisitor;
using NUnit.Framework;

namespace UnitTestProjectForModuleTwo
{
    [TestFixture]
    public class FileSystemVisitorTests
    {
        private FileSystemVisitor CreateVisitor()
        {
            return new FileSystemVisitor(@"C:\Mentoring");
        }

        [Test]
        public void Test_WrongPath()
        {
            Assert.Catch<DirectoryNotFoundException>(() => new FileSystemVisitor("aaa"));
        }
        [Test]
        public void Test_EmptyPath()
        {
            Assert.Catch<ArgumentException>(() => new FileSystemVisitor(""));
        }
        [Test]
        public void Test_NullPath()
        {
            Assert.Catch<ArgumentException>(() => new FileSystemVisitor(null));
        }
        [Test]
        public void Test_AmountOfFiles()
        {
            int count = 0;
            var visitor = CreateVisitor();

            foreach (var v in visitor.StartParsing())
            {
                count += 1;
            }

            Assert.AreEqual(count, 6);
        }
        [Test]
        public void Test_AmountOfHiddenFiles()
        {
            var count = 0;
            var visitor = CreateVisitor();
            visitor.FilterAttributes = FileAttributes.Hidden;

            foreach (var v in visitor.StartParsing())
            {
                count += 1;
            }

            Assert.AreEqual(count, 1);
        }
    }
}
