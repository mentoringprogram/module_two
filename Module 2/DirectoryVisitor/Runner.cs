﻿using System;

namespace DirectoryVisitor
{
    public class Runner
    {
        static void Main(string[] args)
        {
            //Enter your path
            var watcher = new FileSystemVisitor(@"C:\");

            foreach (var iter in watcher.StartParsing())
            {
                Console.WriteLine(iter);
            }

        }
    }
}
