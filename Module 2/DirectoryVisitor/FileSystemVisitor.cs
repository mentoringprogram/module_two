﻿using System;
using System.Collections;
using System.Linq;
using System.IO;
using System.Net.Mime;
using System.Text;
using System.Text.RegularExpressions;


namespace DirectoryVisitor
{
    public class FileSystemVisitor
    {
        private readonly string _directoryPath;

        /// <summary>
        /// Regular Expression for Validation
        /// </summary>
        private const string Regexp = @"^[a-zA-Z]:\\(((?![<>:""/\\|?*]).)+((?<![ .])\\)?)*$";

        /// <summary>
        /// Attributes for validation, for example, Archives, Directories, Hiddens 
        /// </summary>
        public FileAttributes FilterAttributes { get; set; }

        /// <summary>
        /// Constructor that checks the path
        /// </summary>
        /// <param name="directoryPath">Directory path</param>
        public FileSystemVisitor(string directoryPath)
        {
            if (string.IsNullOrEmpty(directoryPath))
            {
                throw new ArgumentException("The path is null or empty");
            }

            var match = Regex.Match(directoryPath, Regexp, RegexOptions.IgnoreCase);

            if (!match.Success)
            {
                throw new DirectoryNotFoundException("The directory path is wrong");
            }

            _directoryPath = directoryPath;
        }

        /// <summary>
        /// Constructor that uses the base constructor and adds attributes
        /// </summary>
        /// <param name="directoryPath">Directory path</param>
        /// <param name="filter">Filters attributes</param>
        public FileSystemVisitor(string directoryPath, FileAttributes filter) : this(directoryPath)
        {
            FilterAttributes = filter;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>Returns an iterator</returns>
        public IEnumerable StartParsing()
        {
            using (var watcher = new FileSystemWatcher())
            { 
                watcher.Path = _directoryPath;

                //Add event handlers
                watcher.Changed += OnChanged;
                watcher.Created += OnChanged;
                watcher.Deleted += OnChanged;
                watcher.Renamed += OnChanged;

                // Begin watching
                watcher.EnableRaisingEvents = true;

                Console.WriteLine("START");
                var directoryInfo = new DirectoryInfo(_directoryPath);
                foreach (var walk in WalkDirectoryTree(directoryInfo, 2))
                {
                    yield return walk;
                }

                // Wait for the user to quit the program.
                Console.WriteLine("Press 'q' to quit the sample.");
                while (Console.Read() != 'q') ;

                Console.WriteLine("FINISH");
            }
        }

        private IEnumerable WalkDirectoryTree(DirectoryInfo root, int amountOfSpaces)
        {
            FileInfo[] files = null;
            Console.WriteLine($"Folder Name: {root.Name}");

            var spaceBuilder = new StringBuilder();

            for (var i = 0; i < amountOfSpaces; i++)
            {
                spaceBuilder.Append("___");
            }

            try
            {
                //The algorithm is given as delegate/lambda
                files = root.GetFiles().Where(f => f.Attributes.HasFlag(FilterAttributes)).ToArray();
            }
            catch (UnauthorizedAccessException e)
            {
                Console.WriteLine(e.Message);
            }

            catch (DirectoryNotFoundException e)
            {
                Console.WriteLine(e.Message);
            }

            //Print names of directories
            if (files != null)
            {
                foreach (var fi in files)
                {
                   yield return $"|{spaceBuilder}{fi.Name}";
                }
            }
            var subDirs = root.GetDirectories();

            //recursively walk through the directory
            foreach (var dirInfo in subDirs)
            {
                foreach (var walk in WalkDirectoryTree(dirInfo, amountOfSpaces + 1))
                {
                    yield return walk;
                }
            }
        }

        // The event handlers
        private void OnChanged(object source, FileSystemEventArgs e)
        {
            // Specify what is done when a file is changed, created, or deleted.
            foreach (var iter in StartParsing())
            {
                Console.WriteLine(iter);
            }
        }
    }
}
